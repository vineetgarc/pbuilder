# Brazilian Portuguese translation of pbuilder.
# Copyright (C) 2011 THE pbuilder COPYRIGHT HOLDER
# This file is distributed under the same license as the pbuilder package.
# Flamarion Jorge <jorge.flamarion@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: pbuilder 0.203\n"
"Report-Msgid-Bugs-To: pbuilder@packages.debian.org\n"
"POT-Creation-Date: 2008-03-07 00:09+0900\n"
"PO-Revision-Date: 2011-11-27 11:07-0200\n"
"Last-Translator: Flamarion Jorge <jorge.flamarion@gmail.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. Type: string
#. Description
#: ../pbuilder.templates:1001
msgid "Default mirror site:"
msgstr "Localização do espelho de rede padrão:"

#. Type: string
#. Description
#: ../pbuilder.templates:1001
msgid "Please enter the default mirror you want to be used by pbuilder."
msgstr ""
"Por favor, informe o espelho de rede padrão que você quer que seja usado "
"pelo pbuilder."

#. Type: string
#. Description
#: ../pbuilder.templates:1001
msgid ""
"If you leave this field blank, there will be one attempt to autodetect this "
"information. If this attempt fails, you will be prompted again to insert "
"some valid mirror information."
msgstr ""
"Se você deixar este campo em branco, será feita uma tentativa de auto "
"detectar esta informação. Se a tentativa falhar, você será perguntado "
"novamente para inserir alguma informação válida de espelho de rede."

#. Type: string
#. Description
#: ../pbuilder.templates:1001
msgid "Here is a valid mirror example: http://deb.debian.org/debian"
msgstr ""
"Este é um exemplo válido de espelho de rede: http://deb.debian.org/debian"

#. Type: error
#. Description
#: ../pbuilder.templates:2001
msgid "Default mirror not found"
msgstr "Espelho de rede padrão não encontrado"

#. Type: error
#. Description
#: ../pbuilder.templates:2001
msgid ""
"Mirror information detection failed and the user provided no mirror "
"information."
msgstr ""
"A detecção da informação do espelho de rede falhou e o usuário não forneceu "
"uma informação de espelho de rede."

#. Type: error
#. Description
#: ../pbuilder.templates:2001
msgid "Please enter valid mirror information."
msgstr "Por favor, forneça uma informação de espelho de rede válida."

#. Type: boolean
#. Description
#: ../pbuilder.templates:3001
msgid "Overwrite current configuration?"
msgstr "Sobrescrever a configuração atual?"

#. Type: boolean
#. Description
#: ../pbuilder.templates:3001
msgid ""
"Your system seems to have already pbuilder configuration. Proceeding might "
"discard or overwrite part or the entire pbuilder's configuration."
msgstr ""
"Seu sistema parece já ter uma configuração do pbuilder. Continuar poderá "
"descartar ou sobrescrever parte ou toda a configuração do pbuilder."
